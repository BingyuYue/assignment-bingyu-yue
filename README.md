# Assignment—Bingyu Yue



## Introduction

The purpose of assignment is to perform predictive modelling of the outbound dining problem using a real world dataset of restaurants in Sydney area in 2018. assignment has two parts, Part A is importing and exploring the data, in this part we explore and analyse the dataset to gain insights and answer specific questions. Part B is collating the data and predictive modelling, in this part, we apply predictive modelling to predict/classify the success of restaurants.

## running method

1. Find the project in git link https://gitlab.com/BingyuYue/assignment-bingyu-yue.git
2. Download the file and upload it to jupyter to run it.


## Usage method

1. Open Notebook, Enter PartA.ipynb in Jupyter Notebook. Perform exploratory data analysis (EDA) and draw conclusions. Use graphical modules (matplotlib and seaborn) to answer specific questions.

2. Open Notebook, Enter PartB.ipynb in Jupyter Notebook or preferred environment. Implement data cleaning and feature encoding, prepare data for modelling and then building Linear Regression Models.


3. After successfully running the code for both parts, users will have a comprehensive understanding of the dataset, insights from exploratory data analysis, and the performance of predictive models.

## Contribution

In this file, a comprehensive understanding of the dataset, insights into exploratory data analysis, and the performance of predictive models will be provided, providing more information on data processing.

## Authors and acknowledgment
Bingyu Yue

Email: u3241718@uni.canberra.edu.au

Student ID: u3241718.
